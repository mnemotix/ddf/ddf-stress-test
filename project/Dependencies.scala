import sbt._

object Version {
  lazy val gatlingAmqp = "0.0.3"
  lazy val gatling = "3.1.0"
  lazy val scalaVersion = "2.12.10"
  lazy val scalaTest = "3.0.8"
  lazy val synaptixVersion = "0.1.8-SNAPSHOT"
}

object Dependencies {
  lazy val synaptix = "com.mnemotix" %% "synaptix-core" % Version.synaptixVersion
  lazy val amqpToolkit = "com.mnemotix" %% "synaptix-amqp-toolkit" % Version.synaptixVersion
  lazy val gatlingHighcharts = "io.gatling.highcharts" % "gatling-charts-highcharts" % Version.gatling % "test,it"
  lazy val gatlingAmqp = "ru.tinkoff" %% "gatling-amqp-plugin" % Version.gatlingAmqp % "test,it"
  lazy val gatling = "io.gatling" % "gatling-test-framework" % Version.gatling % "test,it"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest

  val core = Seq(amqpToolkit, gatlingAmqp, gatlingHighcharts, gatling, scalaTest, synaptix)
}

package org.ddf.stress.test.http.scenario

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

import io.gatling.core.Predef.{csv, exec, scenario}
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef.http
import org.ddf.stress.test.helper.{SparqlFormSearch, StressTestConfig}
import io.gatling.core.Predef._
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object GraphDBScenarios {

  lazy val httpProtocol = http
    .baseUrl(StressTestConfig.rdfStoreRootUri)
    .basicAuth(StressTestConfig.rdfStoreUser, StressTestConfig.rdfStorePassword)
    .acceptHeader("text/html,application/x-www-form-urlencoded,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8,application/json,text/javascript,*/*;q=0.01") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  object FormRequest {
    val execute = (query_word: String) => exec((Requests.graphDBQuery((SparqlFormSearch(query_word)))))
  }

  def snSimpleSearch(fileName: String): ScenarioBuilder = {
    val feeder = csv(getClass.getResource(s"/$fileName").getPath).random
    scenario("Scenario 3 - Recherche Simple")
      .feed(feeder)
      .exec(FormRequest.execute("${words}"))
  }
}
package org.ddf.stress.test.http.feeder

import com.mnemotix.synaptix.core.utils.RandomNameGenerator

import scala.util.Random

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object UserAccountFeeder {
  def apply(): Iterator[Map[String, String]] = {
    Iterator.continually(Map(
      "username" ->  s"Jean Luc ${Random.alphanumeric.take(3).mkString} Roger${Random.alphanumeric.take(8).mkString}",
      "nickName" -> RandomNameGenerator.haiku,
      "email" -> s"${Random.alphanumeric.take(20).mkString}@stresstestddf.com",
      "password" -> s"pass${Random.alphanumeric.take(Random.nextInt((2 to 20).size)).mkString}s",
      "geonames" -> s"${Random.nextInt(9999999 - 1000000) + 1000000}"
    ))
  }
}

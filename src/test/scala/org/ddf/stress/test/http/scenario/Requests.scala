package org.ddf.stress.test.http.scenario

import java.net.URLEncoder
import java.nio.charset.StandardCharsets

import io.gatling.core.Predef.StringBody
import io.gatling.http.Predef.http
import io.gatling.core.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder
import org.ddf.stress.test.helper.{CreateLexicalSense, EsFormSearch, FormSeeAlso_Query, FormTopPosts_Query, Form_Query, LexicalSense_Query, Login, Me_Query, RegisterUserAccount, Search_Query, Simple_Query, StressTestConfig}

import scala.util.Random

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Requests {

  lazy val homePage = http("Retrieve home page").get("/")
  lazy val formQuery: String => HttpRequestBuilder = (entry: String) => http("Form_Query").post("/graphql").header("Content-Type", "application/json").body(StringBody(Form_Query(entry))).asJson
  lazy val FormTopPostsQuery = (entry: String) => http("FormTopPosts_Query").post("/graphql").header("Content-Type", "application/json").body(StringBody(FormTopPosts_Query(entry))).asJson
  lazy val meQuery = http("Me").post("/graphql").header("Content-Type", "application/json").body(StringBody(Me_Query())).asJson
  lazy val searchQuery = (entry: String) => http("Search_Query").post("/graphql").header("Content-Type", "application/json").body(StringBody(Search_Query(entry))).asJson
  lazy val formSeeAlso_Query = (entry: String) => http("FormSeeAlso_Query").post("/graphql").header("Content-Type", "application/json").body(StringBody(FormSeeAlso_Query(entry))).asJson
  lazy val lexicalSense_Query = (entry: String) => http("LexicalSense_Query").post("/graphql").header("Content-Type", "application/json").body(StringBody(LexicalSense_Query(entry))).asJson

  lazy val loginPage = http("login page").get("/login")
  lazy val registerUserAccountQuery= (username: String, nickname: String, password: String, email: String, geonames: String) => {
    http("RegisterUserAccount").post("/graphql").header("Content-Type", "application/json").body(StringBody(RegisterUserAccount(username,nickname, password, email, geonames))).asJson
  }
  lazy val loginQuery = (username:String, password:String) => http("Login").post("/graphql").header("Content-Type", "application/json").body(StringBody(Login(username, password))).asJson
  lazy val createLexicalSenseQuery = (formWrittenRep: String, lexinfo: String, definition: String) => http("CreateLexicalSense").post("/graphql").header("Content-Type", "application/json").body(StringBody(CreateLexicalSense(formWrittenRep, lexinfo, definition))).asJson
  lazy val simpleFormQuery = (formWrittenRep: String) => http("SimpleFormQuery").post("/graphql").header("Content-Type", "application/json").body(StringBody(Simple_Query(formWrittenRep))).asJson

  lazy val esFormSearchQuery = (entry:String) => http("esFormSearchQuery").post("/dev-ddf-form/_search").header("Content-Type", "application/json").body(StringBody(EsFormSearch(entry))).asJson
  lazy val graphDBQuery = (qry:String) => http("graphDBQuery").get(s"/repositories/${StressTestConfig.repoName}").queryParam("query",s"${qry}")


  def getRandomElement(list: Seq[String], random: Random): String =
    list(random.nextInt(list.length))

  def utf8Encode(toConvert: String): Array[Byte] =
    new String(toConvert).getBytes(StandardCharsets.UTF_8)

  def encodePath(path: String): String =
    URLEncoder.encode(path, StandardCharsets.UTF_8.toString)
}


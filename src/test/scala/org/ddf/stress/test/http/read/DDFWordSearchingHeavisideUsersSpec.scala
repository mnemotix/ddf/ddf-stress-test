package org.ddf.stress.test.http.read

import com.typesafe.scalalogging.LazyLogging
import io.gatling.core.Predef.heavisideUsers
import io.gatling.core.scenario.Simulation
import org.ddf.stress.test.helper.StressTestConfig
import org.ddf.stress.test.http.scenario.Scenarios
import io.gatling.core.Predef._
import scala.concurrent.duration._
/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DDFWordSearchingHeavisideUsersSpec extends Simulation with LazyLogging {
  logger.info(s"${StressTestConfig.httpUserAtOnce} users will connected during ${StressTestConfig.rampUsersDuration seconds} following a heaviside courbe to search for words that have a definition")
  val scenarios = Scenarios.snRechercheUtilisateurs("query-result.csv").inject(heavisideUsers(StressTestConfig.httpUserAtOnce) during(StressTestConfig.rampUsersDuration seconds)).protocols(Scenarios.httpProtocol)
  setUp(scenarios)
}

package org.ddf.stress.test.http.feeder

import scala.util.Random

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object DefinitionFeeder {

  lazy val liste1 = Seq("Avec", "Considérant", "Où que nous mène", "Eu égard à", "Vu", "En ce qui concerne", "Dans le cas particulier de", "Quelle que soit", "Du fait de", "Tant que durera", "Nonobstant", "Quoi qu’on dise concernant", "Compte tenu de", "Malgré", "Pour réagir face à","Afin de circonvenir à", "Dans le but de pallier à", "Si vous voulez mon avis concernant")
  lazy val liste2 = Seq("la situation", "la conjoncture", "la crise", "l’inertie", "l’impasse", "l’extrémité", "la dégradation des moeurs", "la sinistrose", "la dualité de la situation", "la baisse de confiance" ,"la restriction", "l’orientation", "la difficulté", "l’austérité" ,"cette rigueur", "la morosité", "l’ambiance", "la politique", "la fragilité", "la complexité", "l’inconstance" ,"cette inflexion")
  lazy val liste3 = Seq("présente", "actuelle", "qui nous occupe", "qui est la nôtre", "induite", "conjoncturelle", "contemporaine", "de cette fin de siècle", "de la société","de ces derniers temps", "générale", "induite", "observée" ,"contextuelle", "de l’époque actuelle", "intrinsèque", "que nous constatons")
  lazy val liste4 = Seq("il convient de", "il faut", "on se doit de", "il est préférable de" ,"il serait intéressant de", "il ne faut pas négliger de", "on ne peut se passer de", "il est nécessaire de", "il serait bon de" ,"il faut de toute urgence" ,"je recommande de" ,"je préconise un audit afin de", "il est très important de" ,"il ne faut pas s’interdire de" ,"nous sommes contraints de" ,"je suggère fortement de" ,"je n’exclus pas de", "je vous demande de")
  lazy val liste5 = Seq("étudier", "examiner" ,"ne pas négliger", "prendre en considération", "anticiper", "imaginer", "se préoccuper de", "s’intéresser à" , "avoir à l’esprit", "se remémorer", "se souvenir" ,"favoriser", "uniformiser", "remodeler", "avoir à l’esprit", "gérer", "fédérer", "comprendre", "analyser", "appréhender", "expérimenter", "essayer", "caractériser", "façonner", "revoir", "prendre en compte", "arrêter de stigmatiser", "considérer", "réorganiser", "inventorier")
  lazy val liste6 = Seq("toutes les", "chacune des" ,"la majorité des" ,"l’ensemble des", "la somme des", "la totalité des" ,"la globalité des", "certaines","parmi les" ,"la simultanéité des", "les relations des", "la plus grande partie des", "les principales", "systématiquement les", "précisément les")
  lazy val liste7 = Seq("solutions", "issues", "problématiques", "voies", "problématiques", "alternatives", "choix", "organisations matricielles", "améliorations","ouvertures", "synergies", "actions", "options", "décisions", "modalités", "hypothèses", "stratégies")
  lazy val liste8 = Seq("envisageables","possibles", "s’offrant à nous" ,"de bon sens" ,"envisageables", "du passé", "déjà en notre possession", "s’offrant à nous", "de bon sens", "du futur", "imaginables", "éventuelles", "réalisables", "déclinables", "pertinentes", "que nous connaissons", "évidentes", "optimales", "opportunes", "emblématiques", "draconiennes")
  lazy val liste9 = Seq("à long terme", "à moyen terme", "pour longtemps","à l’avenir","pour le futur", "depuis longtemps", "à court terme", "rapidement", "dans une perspective correcte", "avec toute la prudence requise", "de toute urgence", "même si ce n’est pas facile", "même si nous devons en tirer des conséquences", "très attentivement", "avec beaucoup de recul", "parce que la nature a horreur du vide", "parce que nous ne faisons plus le même métier", "toutes choses étant égales par ailleurs", "et déjà en notre possession", "en prenant toutes les précautions qui s’imposent", "si l’on veut s’en sortir un jour", "parce qu’il est temps d’agir", "parce qu’il s’agit de notre dernière chance", "parce que les mêmes causes produisent les mêmes effets", "parce que nous le valons bien")

  lazy val lexinfoList = Seq("verb", "adverb", "noun", "properNoun", "adjective", "possessiveAdjective", "pronoun",
    "indefinitePronoun", "interrogativePronoun", "possessivePronoun", "personalPronoun","relativePronoun", "article",
    "definiteArticle", "partitiveArticle", "preposition", "conjunction", "interjection","particle")

  def apply(): Iterator[Map[String, String]] = {
    Iterator.continually(Map(
      "formWrittenRep" ->  s"${Random.alphanumeric.take(Random.nextInt((1 to 20).length)).mkString}",
      "definition" -> (s"${getRandomElement(liste1)} ${getRandomElement(liste2)} ${getRandomElement(liste3)} ${getRandomElement(liste4)} ${getRandomElement(liste5)} ${getRandomElement(liste6)} " +
        s"${getRandomElement(liste7)} ${getRandomElement(liste8)} ${getRandomElement(liste9)}"),
      "lexinfo" -> s"${getRandomElement(lexinfoList)}"
    ))
  }

  def getRandomElement(list: Seq[String]): String = list(Random.nextInt(list.length))
}

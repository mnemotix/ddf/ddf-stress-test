package org.ddf.stress.test.http.graphDB

import com.typesafe.scalalogging.LazyLogging
import io.gatling.core.Predef.atOnceUsers
import io.gatling.core.scenario.Simulation
import org.ddf.stress.test.helper.StressTestConfig
import org.ddf.stress.test.http.scenario.{GraphDBScenarios, Scenarios}
import io.gatling.core.Predef._

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class GraphDBWordSearchingAtOnceSpec extends Simulation with LazyLogging {
  logger.info(s"${StressTestConfig.httpUserAtOnce} users connected and search for words that have a definition in graphDB")
  val scenarios = GraphDBScenarios.snSimpleSearch("query-result.csv").inject(atOnceUsers(StressTestConfig.httpUserAtOnce)).protocols(GraphDBScenarios.httpProtocol)
  setUp(scenarios)
}

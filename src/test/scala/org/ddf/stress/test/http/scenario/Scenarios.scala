package org.ddf.stress.test.http.scenario

import io.gatling.core.Predef.{exec, jsonPath}
import io.gatling.http.Predef.{http, status}
import io.gatling.http.Predef._
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.request.builder.HttpRequestBuilder
import org.ddf.stress.test.helper.StressTestConfig
import org.ddf.stress.test.http.feeder.{DefinitionFeeder, UserAccountFeeder}

import scala.util.Random

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Scenarios {

  lazy val random = new Random

  lazy val pause = random.nextInt(100)

  lazy val checkCode: HttpRequestBuilder => HttpRequestBuilder = (request:HttpRequestBuilder) => request.check(status.not(404), status.not(500), status.not(400))
  lazy val graphQLError: HttpRequestBuilder => HttpRequestBuilder = (request:HttpRequestBuilder) => request.check(status.not(404), status.not(500), status.not(400), jsonPath("$.errors[*].statusCode").count.is(0))

  lazy val httpProtocol = http
    .baseUrl(StressTestConfig.baseUrl)
    .basicAuth(StressTestConfig.basicAuthUser, StressTestConfig.basicAuthPassword)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8,application/json,text/javascript,*/*;q=0.01") // Here are the common headers
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  object HomeRequest {
    val execute = (query_word: String) => exec(graphQLError(Requests.formQuery(query_word)).check(jsonPath("$.data.lexicalSenses.edges[*].node.id").findAll.optional.saveAs("senseIDs")))
      .exec(graphQLError(Requests.FormTopPostsQuery(query_word)))
      .exec(graphQLError(Requests.formSeeAlso_Query(query_word)))
      .exec(graphQLError(Requests.searchQuery(query_word)))
  }

  object SimpleHomeRequest {
    val execute = (query_word: String) => exec(graphQLError(Requests.simpleFormQuery(query_word)).check(jsonPath("$.data.lexicalSenses.edges[*].node.id").findAll.optional.saveAs("senseIDs")))
  }

  object CreateAccount {
    val execute = (username: String, nickname: String, password: String, email: String, geonames: String) => exec(Requests.registerUserAccountQuery(username,
      nickname, password, email, geonames).check(status.not(404), status.not(500), status.not(400)))
  }

  def snRechercheUtilisateurs(fileName: String): ScenarioBuilder = {
    val feeder = csv(getClass.getResource(s"/$fileName").getPath).random
    scenario("Scenario 1 - Recherche Utilisateurs")
      .exec(checkCode(Requests.homePage)).pause(pause)
      .repeat(StressTestConfig.repeatScenario) {
        feed(feeder)
          .exec(HomeRequest.execute("${words}"))
          .pause(pause)
          .exec( session => {
            if (session.contains("senseIDs")) {
              val urisLexicalSens: Seq[String] = session("senseIDs").as[Seq[String]]
              val uriLexicalSens = Requests.getRandomElement(urisLexicalSens, random)
              exec(graphQLError(Requests.lexicalSense_Query(uriLexicalSens)))
              session
            }
            else session
          })
          .pause(pause)
          .exec(HomeRequest.execute("${words}"))
    }
  }

  def snSimpleSearch(fileName: String): ScenarioBuilder = {
    val feeder = csv(getClass.getResource(s"/$fileName").getPath).random
    scenario("Scenario 3 - Recherche Simple")
      .feed(feeder)
      .exec(SimpleHomeRequest.execute("${words}"))
  }

  def snCreateDefinition(): ScenarioBuilder = {
    val feeder = UserAccountFeeder()
    val feederDef = DefinitionFeeder()
    scenario("Scenario 2 - Account creation")
      .exec(checkCode(Requests.homePage)).pause(pause)
      .exec(checkCode(Requests.loginPage)).pause(pause)
      .feed(feeder)
      .feed(feederDef)
      .exec(graphQLError(Requests.registerUserAccountQuery("${username}","${nickName}","${password}", "${email}","${geonames}"))).pause(10)
      .exec(graphQLError(Requests.loginQuery("${username}","${password}")).check(bodyString.saveAs("RESPONSE_DATA")))
      .exec(getCookieValue(CookieKey("cookie").saveAs("key")))
      .exec(addCookie(Cookie("cookie", "$key")))
      .exec(graphQLError(Requests.createLexicalSenseQuery("${formWrittenRep}", "${lexinfo}", "${definition}")).check(bodyString.saveAs("RESPONSE_DATA_SENSE_CREATE"))).exec( session => {
      session
    })
  }
}
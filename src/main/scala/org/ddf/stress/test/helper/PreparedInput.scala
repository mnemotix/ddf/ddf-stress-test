package org.ddf.stress.test.helper

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

trait PreparedInput {}

object Form_Query extends PreparedInput {
  lazy val query = """query Form_Query(\n  $first: Int\n  $after: String\n  $formQs: String\n  $filterByPlaceId: ID\n) {\n  lexicalSenses: lexicalSensesForAccurateWrittenForm(\n    formQs: $formQs\n    first: $first\n    after: $after\n    filterByPlaceId: $filterByPlaceId\n  ) {\n    edges {\n      node {\n        id\n        definition\n        ...LexicalSenseCountriesFragment\n        lexicalEntry {\n          id\n          ...LexicalEntryPartOfSpeechListFragment\n          __typename\n        }\n        lexicographicResource {\n          id\n          altLabel\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    pageInfo {\n      startCursor\n      endCursor\n      hasNextPage\n      __typename\n    }\n    __typename\n  }\n}\nfragment LexicalSenseCountriesFragment on LexicalSense {\n  places {\n    edges {\n      node {\n        id\n        ...CountryFragment\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\nfragment CountryFragment on PlaceInterface {\n  id\n  name\n  color\n  ... on City {\n    countryName\n    __typename\n  }\n  ... on State {\n    countryName\n    __typename\n  }\n  __typename\n}\nfragment LexicalEntryPartOfSpeechListFragment on LexicalEntryInterface {\n  ... on WordInterface {\n    partOfSpeech {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on InflectablePOS {\n    number {\n      id\n      prefLabel\n      __typename\n    }\n    gender {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on Verb {\n    transitivity {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on MultiWordExpression {\n    multiWordType {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on Affix {\n    termElement {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n"""

  def apply(entry_form: String): String = {
    s"""{"query":"$query","variables":{"first":10,"formQs":"${entry_form}"}}""".stripMargin
  }
}

object Simple_Query extends PreparedInput {
  lazy val query = """query Forms($first: Int, $qs: String) {\n  forms(qs: $qs first: $first) {\n  edges {\n  node {\n  id\n  writtenRep\n  }\n  }\n  }\n  }""".stripMargin

  def apply(entry_form: String): String = {
    s"""{"query":"$query","variables":{"first":10,"qs":"${entry_form}"}}""".stripMargin
  }
}

object FormTopPosts_Query extends PreparedInput {
  lazy val query ="""query FormTopPosts_Query($formQs: String) {\n  topPostAboutEtymology: topPostAboutEtymologyForAccurateWrittenForm(formQs: $formQs) {\n    id\n    content\n    __typename\n  }\n  topPostAboutForm: topPostAboutFormForAccurateWrittenForm(formQs: $formQs) {\n    id\n    content\n    __typename\n  }\n}\n"""

  def apply(entry_form: String): String = {
    s"""{"operationName":"FormTopPosts_Query","variables":{"formQs":"${entry_form}"},"query":"${query}"}""".stripMargin
  }
}

object Me_Query extends PreparedInput {
  lazy val query = """query Me {\n  me {\n    id\n    avatar\n    nickName\n    userAccount {\n      username\n      ...UserAccountGroupsFragment\n      __typename\n    }\n    place {\n      id\n      ...PlaceFragment\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment PlaceFragment on PlaceInterface {\n  id\n  __typename\n  name\n  countryCode\n  ... on City {\n    countryId\n    countryName\n    stateName\n    __typename\n  }\n  ... on State {\n    countryId\n    countryName\n    __typename\n  }\n}\n\nfragment UserAccountGroupsFragment on UserAccount {\n  isAdmin: isInGroup(userGroupId: \"user-group/AdministratorGroup\")\n  isOperator: isInGroup(userGroupId: \"user-group/OperatorGroup\")\n  isContributor: isInGroup(userGroupId: \"user-group/ContributorGroup\")\n  __typename\n}\n"""

  def apply(): String = {
    s"""{"operationName":"Me","variables":{},"query":"${query}"}""".stripMargin
  }
}

object Search_Query extends PreparedInput {
  lazy val query = """query Search_Query($first: Int, $qs: String) {\n  forms(qs: $qs, first: $first) {\n    edges {\n      node {\n        id\n        writtenRep\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"""

  def apply(entry_form: String): String = {
    s"""{"operationName":"Search_Query","variables":{"first":10,"qs":"${entry_form}"},"query":"${query}"}""".stripMargin
  }
}

object FormSeeAlso_Query extends PreparedInput {
  lazy val query = """query FormSeeAlso_Query($formQs: String) {\n  associativeRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs, filterOnType: associativeRelation) {\n    edges {\n      node {\n        id\n        ...SemanticRelationFormFragment\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  graphicRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs, filterOnType: graphicRelation) {\n    edges {\n      node {\n        id\n        ...SemanticRelationFormFragment\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  morphologicalRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs, filterOnType: morphologicalRelation) {\n    edges {\n      node {\n        id\n        ...SemanticRelationFormFragment\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment SemanticRelationFormFragment on SemanticRelation {\n  id\n  entries {\n    edges {\n      node {\n        id\n        lexicalEntry {\n          id\n          canonicalForm {\n            id\n            writtenRep\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n"""

  def apply(entry_form: String): String = {
    s"""{"operationName":"FormSeeAlso_Query","variables":{"formQs":"$entry_form"},"query":"${query}"}""".stripMargin
  }
}

object LexicalSense_Query extends PreparedInput {
  lazy val query = """query LexicalSense_Query($lexicalSenseId: ID!) {\n  lexicalSense(id: $lexicalSenseId) {\n    id\n    definition\n    validationRatingsCount\n    canUpdate\n    usageExamples {\n      edges {\n        node {\n          id\n          bibliographicalCitation\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    lexicalEntry {\n      id\n      canonicalForm {\n        id\n        writtenRep\n        ...FormCountriesFragment\n        __typename\n      }\n      ...LexicalEntryPartOfSpeechListFragment\n      __typename\n    }\n    lexicographicResource {\n      id\n      prefLabel\n      altLabel\n      __typename\n    }\n    ...LexicalSenseCountriesFragment\n    ...LexicalSenseTagsFragment\n    ...LexicalSenseSemanticRelationFragment\n    ...LexicalSenseTopPostAboutSenseFragment\n    ...LexicalSenseActionsFragment\n    __typename\n  }\n}\n\nfragment LexicalSenseCountriesFragment on LexicalSense {\n  places {\n    edges {\n      node {\n        id\n        ...CountryFragment\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment CountryFragment on PlaceInterface {\n  id\n  name\n  color\n  ... on City {\n    countryName\n    __typename\n  }\n  ... on State {\n    countryName\n    __typename\n  }\n  __typename\n}\n\nfragment LexicalSenseTagsFragment on LexicalSense {\n  domain {\n    id\n    prefLabel\n    __typename\n  }\n  temporality {\n    id\n    prefLabel\n    __typename\n  }\n  register {\n    id\n    prefLabel\n    __typename\n  }\n  connotation {\n    id\n    prefLabel\n    __typename\n  }\n  frequency {\n    id\n    prefLabel\n    __typename\n  }\n  textualGenre {\n    id\n    prefLabel\n    __typename\n  }\n  sociolect {\n    id\n    prefLabel\n    __typename\n  }\n  grammaticalConstraint {\n    id\n    prefLabel\n    __typename\n  }\n  senseToSenseSemanticRelations: semanticRelations(filterOnSenseToSenseTypes: true) {\n    edges {\n      node {\n        id\n        semanticRelationType {\n          id\n          prefLabel\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment LexicalEntryPartOfSpeechListFragment on LexicalEntryInterface {\n  ... on WordInterface {\n    partOfSpeech {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on InflectablePOS {\n    number {\n      id\n      prefLabel\n      __typename\n    }\n    gender {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on Verb {\n    transitivity {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on MultiWordExpression {\n    multiWordType {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  ... on Affix {\n    termElement {\n      id\n      prefLabel\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment FormCountriesFragment on Form {\n  places {\n    edges {\n      node {\n        id\n        ...CountryFragment\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment LexicalSenseSemanticRelationFragment on LexicalSense {\n  semanticRelations(excludeSenseToSenseTypes: true) {\n    edges {\n      node {\n        id\n        semanticRelationType {\n          id\n          prefLabel\n          __typename\n        }\n        entries {\n          edges {\n            node {\n              id\n              lexicalEntry {\n                id\n                canonicalForm {\n                  id\n                  writtenRep\n                  __typename\n                }\n                __typename\n              }\n              lexicographicResource {\n                id\n                prefLabel\n                __typename\n              }\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  __typename\n}\n\nfragment LexicalSenseTopPostAboutSenseFragment on LexicalSense {\n  topPostAboutSense {\n    id\n    content\n    __typename\n  }\n  __typename\n}\n\nfragment LexicalSenseActionsFragment on LexicalSense {\n  validationRatingsCount\n  suppressionRatingsCount\n  ...LexicalSenseValidationRatingFragment\n  ...LexicalSenseReportingRatingFragment\n  ...LexicalSenseSuppressionRatingFragment\n  __typename\n}\n\nfragment LexicalSenseValidationRatingFragment on LexicalSense {\n  validationRatingsCount\n  validationRating {\n    id\n    __typename\n  }\n  __typename\n}\n\nfragment LexicalSenseReportingRatingFragment on LexicalSense {\n  reportingRatingsCount\n  reportingRating {\n    id\n    __typename\n  }\n  __typename\n}\n\nfragment LexicalSenseSuppressionRatingFragment on LexicalSense {\n  suppressionRatingsCount\n  suppressionRating {\n    id\n    __typename\n  }\n  __typename\n}\n"""

  def apply(lexicalSenseId: String): String = {
    s"""{"operationName":"LexicalSense_Query","variables":{"lexicalSenseId":"$lexicalSenseId"},"query":"${query}"}""".stripMargin
  }
}

object RegisterUserAccount extends PreparedInput {
  lazy val mutation = """mutation RegisterUserAccount($username: String!, $password: String!, $email: String!, $nickName: String!, $placeId: ID) {\n  registerUserAccount(input: {username: $username, password: $password, email: $email, nickName: $nickName, personInput: {placeInput: {id: $placeId}}}) {\n    success\n    __typename\n  }\n}\n"""

  def apply(username: String, nickname: String, password: String, email: String, geonames: String): String = {
    s"""{"operationName":"RegisterUserAccount","variables":{"username":"${username}","nickName":"${nickname}","email":"$email","password":"$password","gcu":true,"placeId":"geonames:$geonames"},"query":"${mutation}"}""".stripMargin
  }
}

object Login extends PreparedInput {
  lazy val mutation = """mutation Login($username: String!, $password: String!) {\n  login(input: {username: $username, password: $password}) {\n    success\n    __typename\n  }\n}\n"""

  def apply(username:String, password:String): String = {
    s"""{"operationName":"Login","variables":{"username":"$username","password":"$password"},"query":"$mutation"}"""
  }
}

object CreateLexicalSense extends PreparedInput {
  lazy val mutation = """mutation CreateLexicalSense($definition: String!, $formWrittenRep: String!, $lexicalEntryTypeName: String!, $grammaticalCategoryId: String) {\n  createLexicalSense(input: {definition: $definition, formWrittenRep: $formWrittenRep, lexicalEntryTypeName: $lexicalEntryTypeName, grammaticalCategoryId: $grammaticalCategoryId}) {\n    createdEdge {\n      node {\n        id\n        lexicalEntry {\n          canonicalForm {\n            id\n            writtenRep\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"""

  def apply(formWrittenRep: String, lexinfo: String, definition: String): String = {
    s"""{"operationName":"CreateLexicalSense","variables":{"formWrittenRep":"$formWrittenRep","grammaticalCategoryId":"lexinfo:$lexinfo","definition":"$definition","lexicalEntryTypeName":"Word"},"query":"$mutation"}"""
  }
}

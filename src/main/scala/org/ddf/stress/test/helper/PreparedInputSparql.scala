package org.ddf.stress.test.helper

import java.net.URLEncoder

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object SparqlFormSearch extends PreparedInput {
  def apply(query: String): String = {
    s"""PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
       |SELECT DISTINCT ?uri WHERE {
       | ?uri a ontolex:Form.
       | ?uri ontolex:writtenRep ?writtenRep.
       | FILTER(REGEX(STR(?writtenRep), "$query"))
       |}
       |LIMIT 11""".stripMargin
  }

  def encodePath(path: String): String =
    URLEncoder.encode(path, "UTF-8").replaceAll("\\+", "%20")
}
package org.ddf.stress.test.helper

import com.typesafe.config.ConfigFactory

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object StressTestConfig {

  lazy val gatlingHttpConf = Option(ConfigFactory.load().getConfig("gatling")).getOrElse(ConfigFactory.empty())
  lazy val httpUserAtOnce = gatlingHttpConf.getInt("http.user.at.once")
  lazy val rampUsers = gatlingHttpConf.getInt("http.ramp.users.number")
  lazy val rampUsersDuration = gatlingHttpConf.getInt("http.ramp.users.duration")
  lazy val repeatScenario = gatlingHttpConf.getInt("http.repeat.scenario")
  lazy val baseUrl = gatlingHttpConf.getString("http.baseUrl")
  lazy val basicAuthUser = gatlingHttpConf.getString("http.basicAuth.user")
  lazy val basicAuthPassword = gatlingHttpConf.getString("http.basicAuth.password")

  lazy val masterClusteURI = gatlingHttpConf.getString("es.masterURI")
  lazy val masterClusterUser = gatlingHttpConf.getString("es.masterClusterUser")
  lazy val masterClusterPassword = gatlingHttpConf.getString("es.masterClusterPassword")

  lazy val rdfStoreRootUri = gatlingHttpConf.getString("rdf.rdfStoreRootUri")
  lazy val repoName = gatlingHttpConf.getString("rdf.repoName")
  lazy val rdfStoreUser = gatlingHttpConf.getString("rdf.rdfStoreUser")
  lazy val rdfStorePassword = gatlingHttpConf.getString("rdf.rdfStorePassword")
}
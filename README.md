# DDF TEST

Ce répertoire contient une liste de tests de montée en charge pour l'application ddf 

## Getting Started

Ces instructions permettront d'avoir une copie du projet sur votre machine locale avec la possibilité de les lancer.

### Prerequisites


```
Docker
```

### Installing

* cloner le projet

* créer le container en local

```
docker build -t ddfstresstest:latest
```

* lancer le container en local 

```
docker run -e GCPKEY="ewo....=" ddfstresstest:latest
```

## Variables d'environnements

- BASE_URL : URL de base du site ddf, p.ex : https://dev-ddf-application.mnemotix.com	
- REPEAT_SCENARIO	: Nombre de fois qu'on va répéter le scénario pour la lecture
- USER_AT_ONCE : Nombre d'utilisateurs connectés en même temps		
- RAMP_USERS_NUMBER : Nombre d'utilisateurs qui vont se connectés durant une période	
- RAMP_USERS_DURATION : Temps en seconde de la période pendant laquelle un nombre d'utilisateur va se connecter
- BASICAUTH_USER	: nom d'utilisateur le cas ou le site est sécurisé par une BASICAUTH	
- BASICAUTH_PWD	: mot de passe dans le cas ou le site est sécurisé par une BASICAUTH	
- TEST_CLASS : Nom de la classe à tester
- BUCKET : bucket GCP pour la copie des résultats
- GCPKEY : json du compte de service GCP en base64

### TEST_CLASS

La variable TEST_CLASS peut prendre les valeurs suivantes : 

READ TEST :

- org.ddf.stress.test.http.read.DDFWordSearchingAtOnceNoDefSpe 
- org.ddf.stress.test.http.read.DDFWordSearchingAtOnceNoDefSpec
- org.ddf.stress.test.http.read.DDFWordSearchingHeavisideUsersSpec  
- org.ddf.stress.test.http.read.DDFWordsSearchingRampSpec 

WRITE TEST :

- org.ddf.stress.test.http.write.DDFWordsSearchingRampSpec 
- org.ddf.stress.test.http.write.DDFDefinitionCreationHeavisideUsersSpec
- org.ddf.stress.test.http.write.DDFDefinitionCreationRampSpec

#### DDFWordSearchingAtOnceNoDefSpec 

Ce test va simuler un nombre d'utilisateur qui vont se connecter en parallèle à l'application et effectuer des recherches. Les mots recherchés viennent d'une liste composé à 80% de mots n'ayant pas de définitions dans le DDF

Les variables à spécifier sont :

- BASE_URL 
- REPEAT_SCENARIO
- USER_AT_ONCE
- TEST_CLASS = "org.ddf.stress.test.http.read.DDFWordSearchingAtOnceNoDefSpec"

#### DDFWordSearchingAtOnceSpec

Ce test va simuler un nombre d'utilisateur qui vont se connecter en parallèle à l'application et effectuer des recherches. Les mots recherchés viennent d'une liste composé des mots  ayant le plus de définitions dans le DDF

Les variables à spécifier sont :

- BASE_URL 
- REPEAT_SCENARIO
- USER_AT_ONCE
- TEST_CLASS = "org.ddf.stress.test.http.read.DDFWordSearchingAtOnceSpec"

#### DDFWordSearchingHeavisideUsersSpec

Ce test va permettre de simuler un nombre de connexions sur une période donnée. Le nombre d'utilisateur en // sur l'application  suit une loi normale. Les mots recherchés viennent d'une liste composé des mots  ayant le plus de définitions dans le DDF

Les variables à spécifier sont :

- BASE_URL 
- RAMP_USERS_NUMBER 
- RAMP_USERS_DURATION 
- TEST_CLASS = "org.ddf.stress.test.http.read.DDFWordSearchingHeavisideUsersSpec"

#### DDFWordsSearchingRampSpec

Ce test va permettre de simuler un nombre de connexions sur une période donnée. Le nombre d'utilisateur en // sur l'application  est géré par gatling en assurant toujours le même nombre d'utilisateur connecté sur un moment. Les mots recherchés viennent d'une liste composé des mots  ayant le plus de définitions dans le DDF

Les variables à spécifier sont :

- BASE_URL 
- RAMP_USERS_NUMBER 
- RAMP_USERS_DURATION 
- TEST_CLASS = "org.ddf.stress.test.http.read.DDFWordsSearchingRampSpec"

#### DDFDefinitionCreationAtOnceSpec

Ce test va simuler un nombre d'utilisateur qui vont se connecter en parallèle à l'application, créer un compte et une définition. Les définitions sont des mots français mis aléatoirement pour former un semblant de phrase

Les variables à spécifier sont :

- BASE_URL 
- RAMP_USERS_NUMBER 
- RAMP_USERS_DURATION 
- TEST_CLASS = "org.ddf.stress.test.http.read.DDFDefinitionCreationAtOnceSpec"

#### DDFDefinitionCreationHeavisideUsersSpec

Ce test va permettre de simuler un nombre de connexions sur une période donnée. Le nombre d'utilisateur en // sur l'application  suit une loi normale. Les utilisateurs vont créer un compte et une définition. Les définitions sont des mots français mis aléatoirement pour former un semblant de phrase

Les variables à spécifier sont :

- BASE_URL 
- RAMP_USERS_NUMBER 
- RAMP_USERS_DURATION 
- TEST_CLASS = "org.ddf.stress.test.http.read.DDFDefinitionCreationHeavisideUsersSpec"

#### DDFDefinitionCreationRampSpec

Ce test va permettre de simuler un nombre de connexions sur une période donnée. Le nombre d'utilisateur en // sur l'application  est géré par gatling en assurant toujours le même nombre d'utilisateur connecté sur un moment. Les utilisateurs vont créer un compte et une définition. Les définitions sont des mots français mis aléatoirement pour former un semblant de phrase

Les variables à spécifier sont :

- BASE_URL 
- RAMP_USERS_NUMBER 
- RAMP_USERS_DURATION 
- TEST_CLASS = "org.ddf.stress.test.http.read.DDFDefinitionCreationRampSpec"

## Scénarios

### lecture

- Utilisateur se connecte à https://dev-ddf-application.mnemotix.com/
- Utilisateur attend entre 1 et 100 secondes
- Utilisateur cherche un mot. Les requêtes lancées : "FormTopPosts_Query", "Form_Query", "Search_Query", "FormSeeAlso_Query"
- Utilisateur attend entre 1 et 100 secondes
- Utilisateur clique sur une définition. Les requêtes lancées : "FormTopPosts_Query", "LexicalSense_Query"
- Utilisateur attend entre 1 et 100 secondes
- Utilisateur clique sur retour à la liste de définition. requêtes lancées point 3 et le processus recommence X fois.


### écriture

- https://dev-ddf-application.mnemotix.com/
- Utilisateur attend entre 1 et 100 secondes
- https://dev-ddf-application.mnemotix.com/login
- Utilisateur attend entre 1 et 100 secondes
- https://dev-ddf-application.mnemotix.com/subscribe. Les requêtes lancées : "RegisterUserAccount"
- Utilisateur attend entre 10 secondes
- Utilisateur se connecte à l'aide de son mot de passe et de son identiant.  Les requêtes lancées :  "login"
- https://dev-ddf-application.mnemotix.com/sense/new : requêtes lancées : "CreateLexicalSense"
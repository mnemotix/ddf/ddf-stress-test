import Dependencies._
import sbt.url

ThisBuild / scalaVersion := Version.scalaVersion
ThisBuild / version := Version.synaptixVersion
ThisBuild / organization := "com.mnemotix"
ThisBuild / organizationName := "MNEMOTIX SCIC"

val meta = """META.INF(.)*""".r

val repoUser = sys.env.get("REPO_USR").getOrElse("")
val repoPass = sys.env.get("REPO_PWS").getOrElse("")

lazy val root = (project in file("."))
  .settings(
    name := "ddf-stress-test",
    homepage := Some(url("http://www.mnemotix.com")),
    licenses := Seq("Apache 2.0" -> url("https://opensource.org/licenses/Apache-2.0")),
    developers := List(
      Developer(
        id = "ndelaforge",
        name = "Nicolas Delaforge",
        email = "nicolas.delaforge@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
      Developer(
        id = "prlherisson",
        name = "Pierre-René Lherisson",
        email = "pr.lherisson@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
      Developer(
        id = "mrogelja",
        name = "Mathieu Rogelja",
        email = "mathieu.rogelja@mnemotix.com",
        url = url("http://www.mnemotix.com")
      )
    ),
    javaOptions in Gatling := overrideDefaultJavaOptions("-Xms1024m", "-Xmx2048m"),
    //credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
    credentials += Credentials("Sonatype Nexus Repository Manager", "nexus.mnemotix.com", repoUser, repoPass),
    pomIncludeRepository := { _ => false },
    publishArtifact in (Test, packageBin) := true,
    publishArtifact := true,
    publishMavenStyle := true,
    packageOptions in assembly ~= { os => os filterNot {_.isInstanceOf[Package.MainClass]} },
    publishTo in ThisBuild := {
      val nexus = "https://nexus.mnemotix.com/repository"
      if (isSnapshot.value) Some("MNX Nexus" at nexus + "/maven-snapshots/")
      else Some("MNX Nexus" at nexus + "/maven-releases/")
    },
    resolvers ++= Seq(
      Resolver.mavenLocal,
      Resolver.sonatypeRepo("public"),
      Resolver.typesafeRepo("releases"),
      Resolver.typesafeIvyRepo("releases"),
      Resolver.sbtPluginRepo("releases"),
      Resolver.bintrayRepo("owner", "repo"),
      "MNX Nexus (releases)" at "https://nexus.mnemotix.com/repository/maven-releases/",
      "MNX Nexus (snapshots)" at "https://nexus.mnemotix.com/repository/maven-snapshots/"
    ),
    libraryDependencies ++= core
  )
  .enablePlugins(GatlingPlugin)

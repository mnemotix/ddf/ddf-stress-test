# DDF TEST RAPPORT

Ce fichier contient un rapport sur des tests gatling réalisés sur l'infrastructure dev ddf. Trois tests
ont été réalisés. L'objectif était de tester le temps de réponse de trois composantes de la stack ddf en 
injectant la même requête à différent niveau de cette stack. 

## LE PROTOCOL

* 400 utilisateurs se connectent de manière simultané
* Chaque utilisateur recherche de manière aléatoire un mot issu d'une liste de 1000 mots

## TEST GraphDB

TEST_CLASS=org.ddf.stress.test.http.graphDB.GraphDBWordSearchingAtOnceSpec

Requête SPARQL :

```
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
SELECT DISTINCT ?uri WHERE {
 ?uri a ontolex:Form.
 ?uri ontolex:writtenRep ?writtenRep.
 FILTER(REGEX(STR(?writtenRep), "$writtenRep"))
}
LIMIT 11
```

Résultats :

```
   30/04/2020 11:12:46================================================================================
   30/04/2020 11:12:462020-04-30 09:12:46                                          25s elapsed
   30/04/2020 11:12:46---- Requests ------------------------------------------------------------------
   30/04/2020 11:12:46> Global                                                   (OK=400    KO=0     )
   30/04/2020 11:12:46> graphDBQuery                                             (OK=400    KO=0     )
   30/04/2020 11:12:46
   30/04/2020 11:12:46---- Scenario 3 - Recherche Simple ---------------------------------------------
   30/04/2020 11:12:46[##########################################################################]100%
   30/04/2020 11:12:46          waiting: 0      / active: 0      / done: 400
   30/04/2020 11:12:46================================================================================
   
   
   
   30/04/2020 11:12:47================================================================================
   30/04/2020 11:12:47---- Global Information --------------------------------------------------------
   30/04/2020 11:12:47> request count                                        400 (OK=400    KO=0     )
   30/04/2020 11:12:47> min response time                                    187 (OK=187    KO=-     )
   30/04/2020 11:12:47> max response time                                  23464 (OK=23464  KO=-     )
   30/04/2020 11:12:47> mean response time                                 15231 (OK=15231  KO=-     )
   30/04/2020 11:12:47> std deviation                                       6040 (OK=6040   KO=-     )
   30/04/2020 11:12:47> response time 50th percentile                      16344 (OK=16344  KO=-     )
   30/04/2020 11:12:47> response time 75th percentile                      20548 (OK=20548  KO=-     )
   30/04/2020 11:12:47> response time 95th percentile                      22930 (OK=22930  KO=-     )
   30/04/2020 11:12:47> response time 99th percentile                      23419 (OK=23419  KO=-     )
   30/04/2020 11:12:47> mean requests/sec                                 15.385 (OK=15.385 KO=-     )
   30/04/2020 11:12:47---- Response Time Distribution ------------------------------------------------
   30/04/2020 11:12:47> t < 800 ms                                             1 (  0%)
   30/04/2020 11:12:47> 800 ms < t < 1200 ms                                   1 (  0%)
   30/04/2020 11:12:47> t > 1200 ms                                          398 (100%)
   30/04/2020 11:12:47> failed                                                 0 (  0%)
   30/04/2020 11:12:47================================================================================
```

## TEST ES

TEST_CLASS=org.ddf.stress.test.http.es.ESWordSearchingAtOnceSpec	

Requête ES :

```
query Forms($first: Int, $qs: String) {
  forms(qs: $qs first: $first) {
    edges {
      node {
        id
        writtenRep
      }
    }
  }
}
Variables : {"first":10,"qs":"$writtenRep"}}
```

Résultats : 

```
30/04/2020 11:26:38================================================================================
30/04/2020 11:26:382020-04-30 09:26:38                                           5s elapsed
30/04/2020 11:26:38---- Requests ------------------------------------------------------------------
30/04/2020 11:26:38> Global                                                   (OK=400    KO=0     )
30/04/2020 11:26:38> esFormSearchQuery                                        (OK=400    KO=0     )
30/04/2020 11:26:38
30/04/2020 11:26:38---- Scenario 3 - Recherche Simple ---------------------------------------------
30/04/2020 11:26:38[##########################################################################]100%
30/04/2020 11:26:38          waiting: 0      / active: 0      / done: 400
30/04/2020 11:26:38================================================================================



30/04/2020 11:26:40================================================================================
30/04/2020 11:26:40---- Global Information --------------------------------------------------------
30/04/2020 11:26:40> request count                                        400 (OK=400    KO=0     )
30/04/2020 11:26:40> min response time                                    995 (OK=995    KO=-     )
30/04/2020 11:26:40> max response time                                   3697 (OK=3697   KO=-     )
30/04/2020 11:26:40> mean response time                                  3110 (OK=3110   KO=-     )
30/04/2020 11:26:40> std deviation                                        443 (OK=443    KO=-     )
30/04/2020 11:26:40> response time 50th percentile                       3144 (OK=3144   KO=-     )
30/04/2020 11:26:40> response time 75th percentile                       3408 (OK=3408   KO=-     )
30/04/2020 11:26:40> response time 95th percentile                       3621 (OK=3621   KO=-     )
30/04/2020 11:26:40> response time 99th percentile                       3677 (OK=3677   KO=-     )
30/04/2020 11:26:40> mean requests/sec                                 66.667 (OK=66.667 KO=-     )
30/04/2020 11:26:40---- Response Time Distribution ------------------------------------------------
30/04/2020 11:26:40> t < 800 ms                                             0 (  0%)
30/04/2020 11:26:40> 800 ms < t < 1200 ms                                   1 (  0%)
30/04/2020 11:26:40> t > 1200 ms                                          399 (100%)
30/04/2020 11:26:40> failed                                                 0 (  0%)
30/04/2020 11:26:40================================================================================
```

## TEST GRAPHQL

TEST_CLASS=org.ddf.stress.test.http.read.DDFSimpleWordSearchingSpec	

Requête GRAPHQL :

```
query Forms($first: Int, $qs: String) {
  forms(qs: $qs first: $first) {
    edges {
      node {
        id
        writtenRep
      }
    }
  }
}
Variables : {"first":10,"qs":"$writtenRep"}}
```

Résultats :

```
30/04/2020 12:09:07================================================================================
30/04/2020 12:09:072020-04-30 10:09:07                                           6s elapsed
30/04/2020 12:09:07---- Requests ------------------------------------------------------------------
30/04/2020 12:09:07> Global                                                   (OK=400    KO=0     )
30/04/2020 12:09:07> SimpleFormQuery                                          (OK=400    KO=0     )
30/04/2020 12:09:07
30/04/2020 12:09:07---- Scenario 3 - Recherche Simple ---------------------------------------------
30/04/2020 12:09:07[##########################################################################]100%
30/04/2020 12:09:07          waiting: 0      / active: 0      / done: 400
30/04/2020 12:09:07================================================================================



30/04/2020 12:09:09================================================================================
30/04/2020 12:09:09---- Global Information --------------------------------------------------------
30/04/2020 12:09:09> request count                                        400 (OK=400    KO=0     )
30/04/2020 12:09:09> min response time                                    356 (OK=356    KO=-     )
30/04/2020 12:09:09> max response time                                   5376 (OK=5376   KO=-     )
30/04/2020 12:09:09> mean response time                                  3877 (OK=3877   KO=-     )
30/04/2020 12:09:09> std deviation                                        997 (OK=997    KO=-     )
30/04/2020 12:09:09> response time 50th percentile                       3699 (OK=3699   KO=-     )
30/04/2020 12:09:09> response time 75th percentile                       4749 (OK=4749   KO=-     )
30/04/2020 12:09:09> response time 95th percentile                       5157 (OK=5157   KO=-     )
30/04/2020 12:09:09> response time 99th percentile                       5309 (OK=5309   KO=-     )
30/04/2020 12:09:09> mean requests/sec                                 57.143 (OK=57.143 KO=-     )
30/04/2020 12:09:09---- Response Time Distribution ------------------------------------------------
30/04/2020 12:09:09> t < 800 ms                                             1 (  0%)
30/04/2020 12:09:09> 800 ms < t < 1200 ms                                   1 (  0%)
30/04/2020 12:09:09> t > 1200 ms                                          398 (100%)
30/04/2020 12:09:09> failed                                                 0 (  0%)
30/04/2020 12:09:09================================================================================
```

## IMAGE RÉSULTATS

![result](Screenshot-result-test-400-users.png)


#!/bin/bash


TS=`date +%Y-%m-%d-%H-%M-%S`
FILE=gatling-result-${INFRA}-${TEST_CLASS}-${TS}.tar.gz
BUCKET="gatling-results-7fec3e2798a7"
TS=`date +%Y-%m-%d-%H%M%S`
FILE=gatling-result-${TS}.tar.gz

if [ -z ${GCPKEY} ]; then echo "GCP key is unset"; else echo "GCP key is set"; fi
echo ${GCPKEY} | base64 -d > key.json

echo "Autenticate to GCP"
gcloud auth activate-service-account --key-file=key.json

echo "Running Gatling"
sbt clean reload update compile "gatling:testOnly ${TEST_CLASS}"

echo "Creating an archive with results"
tar czf ${FILE} target/gatling

echo "Pushing result to object storage"
gsutil cp ${FILE} gs://${BUCKET}

echo ${FILE} "pushed to GCP"
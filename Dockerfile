FROM mozilla/sbt
ENV INFRA = "mnx"
ENV TEST_CLASS="org.ddf.stress.test.http.read.DDFWordSearchingAtOnceSpec"
WORKDIR /app
ADD . /app
RUN \
  apt-get update && \
  apt-get install -y apt-transport-https ca-certificates gnupg && \
  echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
  apt-get update && apt-get install -y google-cloud-sdk && \
  rm -rf /var/lib/apt/lists/* 
ENTRYPOINT ["/app/entrypoint.sh"]